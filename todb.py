try:
    import pymongo
    import csv
    import pandas as pd 
    import json
except:
    print("error while importing")

mongoClient = pymongo.MongoClient("mongodb://localhost:27017/") 

db = mongoClient["mydb"]

dbCollection = db["temp"]
df = pd.read_csv('climate.csv',encoding = 'ISO-8859-1', delimiter=';')   
df.to_json('yourjson.json')                               
jdf = open('yourjson.json').read()                      
data = json.loads(jdf)

res= dbCollection.insert_one(data)

print("Recorded", res.inserted_id)   
